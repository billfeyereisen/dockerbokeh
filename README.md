# README #

Small example of running a bokeh server app on openshift


### How do I get set up? ###
	Install openshift CLI tool 
	clone this repo to local machine
	cd into local repo
	login to openshift oc login (host)
	create a new app: oc create-app . --name=[app name]
	open openshift web console, create a new route 
	edit the Dockerfile CMD line to have the --allow-websocket-origin to have same route as route of app and push change
	rebuild app (with changed file)