FROM continuumio/anaconda

ENV BOKEH_APP app.py
ENV APP_URL bokeh-demo
ENV APP_PORT 5006

EXPOSE 5006

WORKDIR ./app

ADD app.py /app/
ADD entrypoint.sh  /app/
ADD requirements.txt /app/


RUN pip install --trusted-host pypi.python.org -r requirements.txt
CMD bokeh serve --allow-websocket-origin bokehdemo-bokeh-embed.ocpt-usc.bsci.com $BOKEH_APP
#CMD["entrypoint.sh"]